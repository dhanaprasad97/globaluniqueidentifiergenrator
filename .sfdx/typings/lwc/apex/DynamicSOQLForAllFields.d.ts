declare module "@salesforce/apex/DynamicSOQLForAllFields.getMOBILEvarioRoleList" {
  export default function getMOBILEvarioRoleList(param: {mobileVarioRoleId: any}): Promise<any>;
}
